#ifndef TV_RENAME_HPP
#define TV_RENAME_HPP

#include <map>
#include <set>
#include <tuple>
#include <vector>

#include "network.hpp"

// TV flags
#define TV_CHDIR 0x0100
#define TV_TRUST 0x0200
#define TV_LINUX 0x0400
#define TV_DVD 0x0800

#ifdef _WIN32

using string = std::wstring;
using char_t = wchar_t;

#else

using string = std::string;
using char_t = char;

#endif

void singleSeason( const string &path, const string &show, int season,
                   string id, const string &language, const string &pattern,
                   const bool &unix_names, const bool &trust,
                   std::map< int, string > *files_ptr = nullptr,
                   bool print = true, bool dvd = false );

void singleSeason( const string &path, const string &show, int season,
                   string id, const string &language, const string &pattern,
                   const size_t &flags,
                   std::map< int, string > *files_ptr = nullptr,
                   bool print = true );

#ifdef GUI

std::vector< std::tuple< int, string, string, string > >
getRenamedFiles( const string &show, int season, const string &id,
                 const string &language, const string &pattern,
                 const bool &unix_names, const std::map< int, string > &files,
                 bool dvd = false );

#ifndef _WIN32
std::map< string, string > getLangs();
#else
std::vector< std::pair< string, string > > getLangs();
#endif

#else

void multipleSeasons( const string &path, const string &show,
                      const std::set< int > &seasons, const string &language,
                      const string &pattern, const size_t &flags );

void allSeasons( const string &path, const string &show, const string &language,
                 const string &pattern, const size_t &flags );

string getShowId( const string &show, const string &language );

#endif

void printLangs();

bool findLanguage( const char_t *language );

bool validID( const string &id );

bool authenticate( const std::string &api_key );

string showNameFromId( const string &id, const string &language );

std::vector< std::pair< string, string > > searchShow( const string &show,
                                                       const string &language );

#endif // TV_RENAME_HPP
