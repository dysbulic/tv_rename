#ifndef GTKMM_MAIN_WINDOW
#define GTKMM_MAIN_WINDOW

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/combobox.h>
#include <gtkmm/container.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/layout.h>
#include <gtkmm/menubar.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/window.h>
#include <set>

#include "../network.hpp"
#include "seasonwindow.hpp"

class MainWindow : public Gtk::Window {
public:
    explicit MainWindow( const Glib::RefPtr< Gtk::Application > &ptr );
    virtual ~MainWindow();

private:
    void quit();
    void process();
    void getNames();
    void finishedSelection();
    void chooseFile();
    void patternHelp();
    void dbUpdate();
    void dbClean();
    void dbRefresh();
    void dbAdd();
    void dbManage();
    void dbPattern();

protected:
    Gtk::Button *m_button_rename = new Gtk::Button();
    Gtk::Button *m_button_db_add = new Gtk::Button();

    Gtk::Label *m_label_possible = new Gtk::Label();

    Gtk::CheckButton *m_check_linux = new Gtk::CheckButton();
    Gtk::CheckButton *m_check_trust = new Gtk::CheckButton();
    Gtk::CheckButton *m_check_dvd = new Gtk::CheckButton();

    Gtk::ComboBox *m_combo_language = new Gtk::ComboBox();
    Gtk::ComboBox *m_combo_possible = new Gtk::ComboBox();

    Gtk::Entry *m_entry_show = new Gtk::Entry();
    Gtk::Entry *m_entry_dir = new Gtk::Entry();
    Gtk::Entry *m_entry_pattern = new Gtk::Entry();

    class LanguageColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        LanguageColumns() {
            add( m_col_code );
            add( m_col_language );
        }
        Gtk::TreeModelColumn< std::string > m_col_code;
        Gtk::TreeModelColumn< std::string > m_col_language;
    };

    class ShowColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        ShowColumns() {
            add( m_col_id );
            add( m_col_show );
        }
        Gtk::TreeModelColumn< std::string > m_col_id;
        Gtk::TreeModelColumn< std::string > m_col_show;
    };

    LanguageColumns m_columns_language;
    ShowColumns m_columns_show;

    Glib::RefPtr< Gtk::Application > app;

    std::unique_ptr< SeasonWindow > sw{ nullptr };
    std::vector< int > selected;
    std::map< int, std::map< int, string > > files;
    std::string path;
    std::string language_code;
    std::string default_pattern;
    std::map< string, string > language_map;
};

#endif // GTKMM_MAIN_WINDOW
