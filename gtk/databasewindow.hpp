#ifndef GTKMM_DATABASE_WINDOW
#define GTKMM_DATABASE_WINDOW

#include <gtkmm/cellrenderercombo.h>
#include <gtkmm/treestore.h>
#include <gtkmm/treeview.h>
#include <gtkmm/window.h>
#include <unordered_set>

#include "searchwindow.hpp"

class DatabaseWindow : public Gtk::Window {
public:
    DatabaseWindow() = delete;
    DatabaseWindow( bool _unix_names,
                    std::map< std::string, std::string > &_language_map,
                    Glib::RefPtr< Gtk::Application > _app );
    virtual ~DatabaseWindow();

private:
    void save();
    void remove();
    void changed( const Gtk::TreeModel::Path & /*UNUSED*/,
                  const Gtk::TreeModel::iterator &row );
    void quit();
    bool treeViewClick( GdkEventButton *event );
    void languageChange( const Glib::ustring &str, const Gtk::TreeIter &it );
    void finishedSearch();

    void errorPath( const std::string &path );
    void errorID( const std::string &show_id );
    void errorLanguage( const std::string &lang );

    std::unordered_set< size_t > changed_rows;

protected:
    Gtk::TreeView *m_tree_database = new Gtk::TreeView();

    class LanguageColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        LanguageColumns() {
            add( m_col_code );
            add( m_col_language );
        }
        Gtk::TreeModelColumn< std::string > m_col_code;
        Gtk::TreeModelColumn< std::string > m_col_language;
    };

    class DatabaseColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        DatabaseColumns() {
            add( m_col_id );
            add( m_col_show );
            add( m_col_path );
            add( m_col_lang );
            add( m_col_lang_full );
            add( m_col_show_id );
            add( m_col_dvd );
        }
        Gtk::TreeModelColumn< size_t > m_col_id;
        Gtk::TreeModelColumn< std::string > m_col_show;
        Gtk::TreeModelColumn< std::string > m_col_path;
        Gtk::TreeModelColumn< std::string > m_col_lang;
        Gtk::TreeModelColumn< std::string > m_col_lang_full;
        Gtk::TreeModelColumn< std::string > m_col_show_id;
        Gtk::TreeModelColumn< bool > m_col_dvd;
    };

    DatabaseColumns m_columns_database;
    LanguageColumns m_columns_language;

    Glib::RefPtr< Gtk::TreeStore > m_model;
    Gtk::CellRendererCombo m_combo_language;

    bool unix_names;
    std::map< std::string, std::string > &language_map;
    Glib::RefPtr< Gtk::Application > app;
    std::unique_ptr< SearchWindow > sw{ nullptr };
    std::string show_search;
    std::string id_search;
    std::string lang_search;
};

#endif // GTKMM_MAIN_WINDOW
