#include "gtkfunctions.hpp"

#include <gtkmm/liststore.h>
#include <gtkmm/messagedialog.h>

#include "../functions.hpp"
#include "../resources_linux.h"
#include "../tv_rename.hpp"

void searchShow( Gtk::Entry *entry_show, Gtk::ComboBox *combo_possible,
                 const Gtk::TreeModelColumn< std::string > &col_show,
                 const Gtk::TreeModelColumn< std::string > &col_id,
                 const std::string &language_code, Gtk::Window *parent ) {
    // check required field is filled out
    if ( entry_show->get_text().empty() ) {
        Gtk::MessageDialog dialog( *parent, _( SHOW_FIELD_EMPTY ) );
        dialog.run();
        return;
    }

    // fill up m_combo_possible with possible tv shows
    auto possible_shows =
        searchShow( std::string( entry_show->get_text() ), language_code );

    // if no possible shows were found, tell the user
    if ( possible_shows.size() == 0 ) {
        Gtk::MessageDialog dialog( *parent, _( NO_RESULTS ) );
        dialog.run();
        return;
    }

    // show widgets
    combo_possible->show();

    // fill up combo box with results from thetvdb
    auto model =
        static_cast< Gtk::ListStore * >( combo_possible->get_model().get() );
    model->clear();

    auto row = *( model->append() );

    row[col_show] = possible_shows[0].first;
    row[col_id] = possible_shows[0].second;
    combo_possible->set_active( row );

    for ( size_t i = 1; i < possible_shows.size(); i++ ) {
        auto row = *( model->append() );
        row[col_show] = possible_shows[i].first;
        row[col_id] = possible_shows[i].second;
    }
}

void freeChildren( std::vector< Gtk::Widget * > &children ) {
    size_t max = children.size();
    size_t index{};
    while ( index < max ) {
        if ( auto *p = dynamic_cast< Gtk::Container * >( children[index] ) ) {
            auto temp = p->get_children();
            children.insert( children.end(), temp.begin(), temp.end() );
            max = children.size();
        }
        index++;
    }
    for ( int i = max - 1; i >= 0; i-- ) {
        delete children[i];
    }
}
