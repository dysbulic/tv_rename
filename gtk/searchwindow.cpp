#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/liststore.h>
#include <gtkmm/messagedialog.h>
#include <iostream>

#include "../functions.hpp"
#include "../resources_linux.h"
#include "../tv_rename.hpp"
#include "gtkfunctions.hpp"
#include "searchwindow.hpp"

void SearchWindow::search() {
    language_code =
        ( *m_combo_language->get_active() )[m_columns_language.m_col_code];

    searchShow( m_entry_show, m_combo_possible, m_columns_show.m_col_show,
                m_columns_show.m_col_id, language_code, this );
}

void SearchWindow::select() {
    search_show =
        ( *m_combo_possible->get_active() )[m_columns_show.m_col_show];
    search_id = ( *m_combo_possible->get_active() )[m_columns_show.m_col_id];
    quit();
}

void SearchWindow::quit() {
    hide();
}

SearchWindow::~SearchWindow() {
    auto children = get_children();
    freeChildren( children );
}

SearchWindow::SearchWindow( std::string &_show, std::string &_id,
                            std::string &_lang,
                            std::map< std::string, std::string > &_languages )
        : search_show( _show ), search_id( _id ), language_code( _lang ),
          language_map( _languages ) {
    set_title( _( GUI_WINDOW_SEARCH ) );
    property_modal().set_value( true );

    auto *box = new Gtk::Box( Gtk::ORIENTATION_VERTICAL );
    add( *box );
    auto *hbox = new Gtk::Box( Gtk::ORIENTATION_HORIZONTAL );
    auto *buttons = new Gtk::Box( Gtk::ORIENTATION_HORIZONTAL );
    box->pack_start( *hbox, Gtk::PACK_EXPAND_WIDGET );
    box->pack_start( *m_combo_possible, Gtk::PACK_SHRINK );
    box->pack_start( *buttons, Gtk::PACK_EXPAND_WIDGET );
    hbox->pack_start( *m_entry_show, Gtk::PACK_EXPAND_WIDGET );
    hbox->pack_start( *m_combo_language, Gtk::PACK_EXPAND_WIDGET );
    auto *search_button = new Gtk::Button( _( SEARCH ) );
    hbox->pack_start( *search_button, Gtk::PACK_SHRINK );
    auto *select_button = new Gtk::Button( _( SELECT ) );
    auto *cancel_button = new Gtk::Button( _( CANCEL ) );
    buttons->pack_end( *cancel_button, Gtk::PACK_SHRINK );
    buttons->pack_end( *select_button, Gtk::PACK_SHRINK );

    hbox->set_margin_top( 5 );
    hbox->set_margin_left( 5 );
    hbox->set_margin_right( 5 );
    m_entry_show->set_margin_right( 5 );
    m_entry_show->set_valign( Gtk::ALIGN_CENTER );
    m_combo_language->set_margin_right( 5 );
    m_combo_language->set_halign( Gtk::ALIGN_CENTER );
    m_combo_language->set_valign( Gtk::ALIGN_CENTER );
    search_button->set_halign( Gtk::ALIGN_CENTER );
    search_button->set_valign( Gtk::ALIGN_CENTER );

    m_combo_possible->set_margin_top( 5 );
    m_combo_possible->set_margin_right( 5 );
    m_combo_possible->set_margin_left( 5 );
    m_combo_possible->set_halign( Gtk::ALIGN_CENTER );
    m_combo_possible->set_valign( Gtk::ALIGN_CENTER );

    buttons->set_margin_top( 5 );
    buttons->set_margin_left( 5 );
    buttons->set_margin_bottom( 5 );
    buttons->set_margin_right( 5 );
    select_button->set_margin_right( 5 );
    select_button->set_halign( Gtk::ALIGN_CENTER );
    select_button->set_valign( Gtk::ALIGN_CENTER );
    cancel_button->set_halign( Gtk::ALIGN_CENTER );
    cancel_button->set_valign( Gtk::ALIGN_CENTER );

    select_button->set_size_request( 80, 30 );
    cancel_button->set_size_request( 80, 30 );

    {
        auto model = Gtk::ListStore::create( m_columns_language );
        m_combo_language->set_model( model );

        for ( const auto &x : language_map ) {
            auto row = *( model->append() );
            row[m_columns_language.m_col_code] = x.first;
            row[m_columns_language.m_col_language] = x.second;
            if ( x.first == "en" )
                m_combo_language->set_active( row );
        }
    }

    {
        auto model = Gtk::ListStore::create( m_columns_show );
        m_combo_possible->set_model( model );
    }

    m_combo_language->pack_start( m_columns_language.m_col_language );
    m_combo_possible->pack_start( m_columns_show.m_col_show );

    search_button->signal_clicked().connect(
        sigc::mem_fun( *this, &SearchWindow::search ) );
    select_button->signal_clicked().connect(
        sigc::mem_fun( *this, &SearchWindow::select ) );
    cancel_button->signal_clicked().connect(
        sigc::mem_fun( *this, &SearchWindow::quit ) );

    // show everything
    show_all_children();
    m_combo_possible->hide();
}
