#ifndef GTK_FUNCTIONS_HPP
#define GTK_FUNCTIONS_HPP

#include <gtkmm/combobox.h>
#include <gtkmm/entry.h>

void searchShow( Gtk::Entry *entry_show, Gtk::ComboBox *combo_possible,
                 const Gtk::TreeModelColumn< std::string > &col_show,
                 const Gtk::TreeModelColumn< std::string > &col_id,
                 const std::string &language_code, Gtk::Window *parent );

void freeChildren( std::vector< Gtk::Widget * > &children );

#endif
