#ifndef GTKMM_SEARCH_WINDOW
#define GTKMM_SEARCH_WINDOW

#include <gtkmm/combobox.h>
#include <gtkmm/treeview.h>
#include <gtkmm/window.h>
#include <unordered_set>

class SearchWindow : public Gtk::Window {
public:
    SearchWindow() = delete;
    SearchWindow( std::string &_show, std::string &_id, std::string &_lang,
                  std::map< std::string, std::string > &_languages );
    virtual ~SearchWindow();

private:
    void search();
    void select();
    void quit();

protected:
    class LanguageColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        LanguageColumns() {
            add( m_col_code );
            add( m_col_language );
        }
        Gtk::TreeModelColumn< std::string > m_col_code;
        Gtk::TreeModelColumn< std::string > m_col_language;
    };

    class ShowColumns : public Gtk::TreeModel::ColumnRecord {
    public:
        ShowColumns() {
            add( m_col_id );
            add( m_col_show );
        }
        Gtk::TreeModelColumn< std::string > m_col_id;
        Gtk::TreeModelColumn< std::string > m_col_show;
    };

    LanguageColumns m_columns_language;
    ShowColumns m_columns_show;

    Gtk::Entry *m_entry_show = new Gtk::Entry();
    Gtk::ComboBox *m_combo_possible = new Gtk::ComboBox();
    Gtk::ComboBox *m_combo_language = new Gtk::ComboBox();

    std::string &search_show;
    std::string &search_id;
    std::string &language_code;
    std::map< std::string, std::string > &language_map;
};

#endif // GTKMM_MAIN_WINDOW
