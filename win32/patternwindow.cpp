#pragma comment( linker, "\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"" )
#include "patternwindow.hpp"
#include "../functions.hpp"
#include "../resources_windows.h"
#include "gui_functions.hpp"

#define ID_PATTERN_STATIC 0x0001

#define ID_PATTERN_EDIT 0x1000
#define ID_OK_BUTTON 0x1001
#define ID_CANCEL_BUTTON 0x1002

PatternWindow *PatternWindow::pw = nullptr;

PatternWindow::PatternWindow( HINSTANCE hInstance, const wchar_t *pattern,
                              HWND parent_window )
        : parent( parent_window ) {
    window = CreateWindowW( L"PatternWindow", _( PATTERN ),
                            WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME | WS_VISIBLE,
                            CW_USEDEFAULT, CW_USEDEFAULT, window_width,
                            window_height, parent, NULL, hInstance, NULL );
    ShowWindow( window, SW_SHOW );
    auto hFont = ( HFONT )GetStockObject( DEFAULT_GUI_FONT );
    EnableWindow( parent, false );
    setIcons( hInstance, window );

    std::wstring label = _( PATTERN );
    label += L":";
    createLabel( label.c_str(), 5, 5, 30, 15, ID_PATTERN_STATIC, window,
                 hFont );
    pattern_input =
        createEditBox( 5, 20, 190, 20, ID_PATTERN_EDIT, window, hFont );

    createButton( _( CANCEL ), window_width - 100, window_height - 88, 80, 25,
                  ID_CANCEL_BUTTON, window, hFont );
    createButton( _( OK ), window_width - 185, window_height - 88, 80, 25,
                  ID_OK_BUTTON, window, hFont );

    SendMessage( pattern_input, WM_SETTEXT, NULL, ( LPARAM )pattern );
    UpdateWindow( window );
}

void PatternWindow::storePattern() {
    SendMessage( pattern_input, WM_GETTEXT, ( WPARAM )2047, ( LPARAM )pattern );
}

const wchar_t *PatternWindow::getPattern() {
    return pattern;
}

LRESULT CALLBACK PatternWindow::messageHandler( HWND hwnd, UINT umsg,
                                                WPARAM wParam, LPARAM lParam ) {
    switch ( umsg ) {
    case WM_CREATE:
        centerWindow( hwnd );
        break;
    case WM_COMMAND:
        switch ( LOWORD( wParam ) ) {
        case ID_OK_BUTTON:
            pw->result = DIA_OK;
            pw->storePattern();
            SendMessage( hwnd, WM_CLOSE, 0, 0 );
            break;
        case ID_CANCEL_BUTTON:
            pw->result = DIA_Cancel;
            SendMessage( hwnd, WM_CLOSE, 0, 0 );
            break;
        }
        break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;
    }
    return DefWindowProcW( hwnd, umsg, wParam, lParam );
}

void PatternWindow::mainLoop() {
    MSG msg;
    while ( GetMessage( &msg, NULL, 0, 0 ) ) {
        if ( !IsDialogMessage( window, &msg ) ) {
            TranslateMessage( &msg );
            pw = this;
            DispatchMessage( &msg );
        }
    }
}

bool PatternWindow::accepted() {
    return result == DIA_OK;
}
