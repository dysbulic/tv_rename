#include "../functions.hpp"
#include "../resources_windows.h"
#include "gui_functions.hpp"
#include "databasewindow.hpp"
#include "mainwindow.hpp"
#include "patternwindow.hpp"
#include "progresswindow.hpp"
#include "searchwindow.hpp"
#include "seasonwindow.hpp"

#include <shlobj.h>

void registerWindowClasses( HINSTANCE hInstance ) {
    WNDCLASSW wc{};
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpszClassName = L"MainWindow";
    wc.hInstance = hInstance;
    wc.hbrBackground = GetSysColorBrush( COLOR_3DFACE );
    wc.lpfnWndProc = MainWindow::messageHandler;
    wc.hCursor = LoadCursor( NULL, IDC_ARROW );
    wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
    RegisterClassW( &wc );

    wc.lpszClassName = L"PatternWindow";
    wc.lpfnWndProc = PatternWindow::messageHandler;
    RegisterClassW( &wc );

    wc.lpszClassName = L"ProgressWindow";
    wc.lpfnWndProc = ProgressWindow::messageHandler;
    RegisterClassW( &wc );

    wc.lpszClassName = L"SeasonWindow";
    wc.lpfnWndProc = SeasonWindow::messageHandler;
    RegisterClassW( &wc );

    wc.lpszClassName = L"DatabaseWindow";
    wc.lpfnWndProc = DatabaseWindow::messageHandler;
    RegisterClassW( &wc );

    wc.lpszClassName = L"SearchWindow";
    wc.lpfnWndProc = SearchWindow::messageHandler;
    RegisterClassW( &wc );
}

void centerWindow( HWND hwnd ) {
    RECT rc = {};

    GetWindowRect( hwnd, &rc );
    int win_w = rc.right - rc.left;
    int win_h = rc.bottom - rc.top;

    int screen_w = GetSystemMetrics( SM_CXSCREEN );
    int screen_h = GetSystemMetrics( SM_CYSCREEN );

    SetWindowPos( hwnd, HWND_TOP, ( screen_w - win_w ) / 2,
                  ( screen_h - win_h ) / 2, 0, 0, SWP_NOSIZE );
}

HWND createWindow( const wchar_t *type, const wchar_t *text, int x, int y,
                   int width, int height, long long id, HWND parent,
                   HFONT hFont, long long dwStyle, long long exStyle ) {
    HWND child_hwnd =
        CreateWindowExW( exStyle, type, text, dwStyle, x, y, width, height,
                         parent, ( HMENU )id, NULL, NULL );
    SendMessage( child_hwnd, WM_SETFONT, ( WPARAM )hFont, true );
    return child_hwnd;
}

HWND createLabel( const wchar_t *text, int x, int y, int width, int height,
                  long long id, HWND parent, HFONT hFont ) {
    return createWindow( L"Static", text, x, y, width, height, id, parent,
                         hFont, WS_VISIBLE | WS_CHILD | SS_LEFT | WS_TABSTOP,
                         0 );
}

HWND createEditBox( int x, int y, int width, int height, long long id,
                    HWND parent, HFONT hFont ) {
    return createWindow( L"Edit", NULL, x, y, width, height, id, parent, hFont,
                         WS_VISIBLE | WS_CHILD | WS_TABSTOP | ES_AUTOHSCROLL,
                         WS_EX_CLIENTEDGE );
}

HWND createCombo( int x, int y, int width, int height, long long id,
                  HWND parent, HFONT hFont ) {
    return createWindow(
        L"Combobox", NULL, x, y, width, height, id, parent, hFont,
        WS_VISIBLE | WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP, 0 );
}

void idealButton( HWND hwnd, int x, int y, int width, int height ) {
    SIZE size;
    SendMessage( hwnd, BCM_GETIDEALSIZE, 0, ( LPARAM )&size );
    auto ideal_width = size.cx + 10;
    if ( ideal_width > width )
        SetWindowPos( hwnd, HWND_TOP, x, y, ideal_width, height, 0 );
}

HWND createButton( const wchar_t *text, int x, int y, int width, int height,
                   long long id, HWND parent, HFONT hFont ) {
    HWND hwnd = createWindow(
        L"Button", text, x, y, width, height, id, parent, hFont,
        WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | WS_TABSTOP, 0 );
    idealButton( hwnd, x, y, width, height );
    return hwnd;
}

HWND createCheckbox( const wchar_t *text, int x, int y, int width, int height,
                     long long id, HWND parent, HFONT hFont ) {
    return createWindow(
        L"Button", text, x, y, width, height, id, parent, hFont,
        WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX | WS_TABSTOP, 0 );
}

HWND createProgressbar( int x, int y, int width, int height, long long id,
                        HWND parent, HFONT hFont ) {
    return createWindow( PROGRESS_CLASSW, NULL, x, y, width, height, id, parent,
                         hFont, WS_CHILD | WS_VISIBLE | PBS_SMOOTH, 0 );
}

HWND createListView( int x, int y, int width, int height, long long id,
                     HWND parent, HFONT hFont ) {
    auto hwnd = createWindow(
        WC_LISTVIEWW, NULL, x, y, width, height, id, parent, hFont,
        WS_CHILD | LVS_REPORT | LVS_ALIGNLEFT | WS_BORDER | WS_VISIBLE, 0 );
    SendMessage( hwnd, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT );
    return hwnd;
}

LVCOLUMNW addColumnToListView( HWND list_view, const wchar_t *text, int width,
                               int pos ) {
    // not thread-safe, but I don't care
    static wchar_t pszText[MAX_PATH];
    static LVCOLUMNW col{};
    wcscpy( pszText, text );

    col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
    col.fmt = LVCFMT_LEFT;
    col.cx = width;
    col.pszText = pszText;
    SendMessage( list_view, LVM_INSERTCOLUMN, pos, ( LPARAM )&col );
    return col;
}

void addItemToCombo( HWND combo, const wchar_t *text ) {
    SendMessage( combo, CB_ADDSTRING, 0, ( LPARAM )text );
}

void addItemToListView( HWND list_view, int row, int column,
                        const wchar_t *text ) {
    // not thread-safe, but I don't care
    static wchar_t pszText[MAX_PATH];
    static LVITEMW item{};
    wcscpy( pszText, text );

    item.mask = LVIF_TEXT;
    item.iItem = row;
    item.iSubItem = column;
    item.pszText = pszText;
    item.cchTextMax = MAX_PATH;
    if ( column == 0 )
        SendMessage( list_view, LVM_INSERTITEM, 0, ( LPARAM )&item );
    else
        SendMessage( list_view, LVM_SETITEMTEXT, item.iItem, ( LPARAM )&item );
}

void setListViewItemText( HWND list_view, int row, int column,
                          const wchar_t *text ) {
    static wchar_t pszText[MAX_PATH];
    static LVITEMW item{};
    wcscpy( pszText, text );

    item.mask = LVIF_TEXT;
    item.iItem = row;
    item.iSubItem = column;
    item.pszText = pszText;
    item.cchTextMax = MAX_PATH;
    SendMessage( list_view, LVM_SETITEMTEXT, row, ( LPARAM )&item );
}

std::wstring getDir() {
    wchar_t path[MAX_PATH];
    BROWSEINFO bi{};
    bi.lpszTitle = _( CHOOSE_DIR );
    LPITEMIDLIST pidl = SHBrowseForFolder( &bi );
    if ( pidl != 0 ) {
        SHGetPathFromIDList( pidl, path );
        IMalloc *imalloc = 0;
        if ( SUCCEEDED( SHGetMalloc( &imalloc ) ) ) {
            imalloc->Free( pidl );
            imalloc->Release();
        }
    } else {
        path[0] = '\0';
    }
    return path;
}

std::wstring getItemText( HWND hwnd, int row, int column ) {
    wchar_t text[MAX_PATH];
    ListView_GetItemText( hwnd, row, column, text, MAX_PATH );
    return text;
}

int getComboCurSel( HWND hwnd ) {
    return SendMessage( hwnd, CB_GETCURSEL, 0, 0 );
}

void setIcons( HINSTANCE hInstance, HWND window ) {
    auto hIcon = LoadIcon( hInstance, MAKEINTRESOURCE( IDI_TV_RENAME_GUI ) );
    auto hIconSmall = LoadIcon( hInstance, MAKEINTRESOURCE( IDI_SMALL ) );
    SendMessage( window, WM_SETICON, ( WPARAM )ICON_BIG, ( LPARAM )hIcon );
    SendMessage( window, WM_SETICON, ( WPARAM )ICON_SMALL,
                 ( LPARAM )hIconSmall );
}
