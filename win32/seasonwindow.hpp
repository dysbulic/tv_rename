#include <string>
#include <vector>
#include <windows.h>

class SeasonWindow {
public:
    SeasonWindow( HINSTANCE hInstance, const std::vector< int > &seasons,
                  std::vector< int > &return_vector, HWND parent_window );

    void mainLoop();
    static LRESULT CALLBACK messageHandler( HWND hwnd, UINT umsg, WPARAM wParam,
                                            LPARAM lParam );
    ~SeasonWindow() {
        EnableWindow( parent, true );
        SetFocus( parent );
    }

private:
    void selectNone();
    void selectAll();
    void storeResult();

    HWND window;
    HWND parent;

    const int window_width{ 265 };
    const int window_height{ 290 };
    std::vector< HWND > checkboxes;
    std::vector< int > &returned;
    const std::vector< int > &season_nums;

    static SeasonWindow *sw;
};
