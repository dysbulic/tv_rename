#pragma comment( linker, "\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"" )
#include "progresswindow.hpp"
#include "../functions.hpp"
#include "../progress.hpp"
#include "../resources_windows.h"
#include "gui_functions.hpp"

#include <commctrl.h>

#define ID_PROGRESS_STATIC 0x0001

#define ID_PROGRESS_BAR 0x1000

ProgressWindow *ProgressWindow::pw = nullptr;

ProgressWindow::ProgressWindow( HINSTANCE hInstance, HWND parent_window )
        : parent( parent_window ) {
    window = CreateWindowW( L"ProgressWindow", _( GUI_WINDOW_PROGRESS ),
                            WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT,
                            CW_USEDEFAULT, window_width, window_height, parent,
                            NULL, hInstance, NULL );
    ShowWindow( window, SW_SHOW );
    auto hFont = ( HFONT )GetStockObject( DEFAULT_GUI_FONT );
    EnableWindow( parent, false );
    setIcons( hInstance, window );

    std::wstring label = _( GUI_WINDOW_PROGRESS );
    label += L":";
    progress_label = createLabel( label.c_str(), 5, 5, window_width - 25, 15,
                                  ID_PROGRESS_STATIC, window, hFont );
    progress_bar = createProgressbar( 5, 20, window_width - 25, 25,
                                      ID_PROGRESS_BAR, window, hFont );
    SendMessage( progress_bar, PBM_SETRANGE, 0, MAKELPARAM( 0, 100 ) );
    SendMessage( progress_bar, PBM_SETPOS, 0, 0 );

    UpdateWindow( window );
}

LRESULT CALLBACK ProgressWindow::messageHandler( HWND hwnd, UINT umsg,
                                                 WPARAM wParam,
                                                 LPARAM lParam ) {
    switch ( umsg ) {
    case WM_CREATE:
        centerWindow( hwnd );
        break;
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;
    case WM_APP:
        switch ( wParam ) {
        case PROGRESS_PERC:
            SendMessage( pw->progress_bar, PBM_SETPOS, ( int )lParam, 0 );
            break;
        case PROGRESS_STRING:
            SendMessage( pw->progress_label, WM_SETTEXT, NULL, lParam );
            break;
        }
    }
    return DefWindowProcW( hwnd, umsg, wParam, lParam );
}

void ProgressWindow::mainLoop() {
    MSG msg;
    while ( GetMessage( &msg, NULL, 0, 0 ) ) {
        if ( !IsDialogMessage( window, &msg ) ) {
            TranslateMessage( &msg );
            pw = this;
            DispatchMessage( &msg );
        }
    }
}

HWND ProgressWindow::getWindow() {
    return window;
}
