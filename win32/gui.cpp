#include <windows.h>
#include "gui_functions.hpp"
#include "mainwindow.hpp"

int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     PWSTR szCmdLine, int nCmdShow ) {
    registerWindowClasses( hInstance );
    MainWindow mw( hInstance, nCmdShow );
    mw.mainLoop();
    return 0;
}
